#!/usr/bin/env python
import sys
from pyqtlib import QtGui, QtCore
from pyqtlib.node_graph import Node, BoxNode, CirclePort, RequiresPort, ProvidesPort
from pyqtlib.node_graph.GraphSceneViewMain import GraphSceneViewMain
from pyqtlib.node_graph import TextItem

from taskforce_view_library.StatusTextView import StatusTextView


class MainWindow(GraphSceneViewMain):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)

        self.statusView = StatusTextView()
        self.scene.addItem(self.statusView)

        self.setCentralWidget(self.view)


if __name__ == "__main__":
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    ex = MainWindow()
    ex.resize(1200,600)
    ex.show()
    sys.exit(app.exec_())
