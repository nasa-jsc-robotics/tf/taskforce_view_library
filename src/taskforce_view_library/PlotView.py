from pyqtlib import QtGui, QtCore

from collections import deque
import pyqtgraph

from pyqtlib.node_graph import ProxyBorder

from taskforce_common import Task
from taskforce_gui import TaskView


class PlotView(TaskView):
    """
    The PlotView is a line plot of data that updates in real-time.
    """
    def __init__(self, size=(300.,300.), pos=(0.0, 0.0), parent=None):
        TaskView.__init__(self, parent)

        # place to store the data
        self.data = deque(maxlen=100)

        # the plot widget
        self.plotWidget = pyqtgraph.PlotWidget()
        self.plotWidget.setAutoPan(True)
        self.plotWidget.setMouseEnabled(True)
        self.plot = self.plotWidget.plot()

        # the proxy is necessary in order to hold the plot widget
        self.proxy = QtGui.QGraphicsProxyWidget()
        self.proxy.setWidget(self.plotWidget)
        self.proxy.resize(QtCore.QSizeF(*size))

        # placing the proxy inside the ProxyBorder will allow the user to resize the plot with the mouse
        self.plotBorder = ProxyBorder()
        self.plotBorder.setProxy(self.proxy)
        self.plotBorder.setPos(*pos)

        # overload the contextMenu event of the border to allow us to link/unlink this view
        self.plotBorder.contextMenuEvent = self.contextMenuEvent
        self.plotBorder.wheelEvent = self.wheelEvent
        self.plotBorder.setParentItem(self)

        if parent:
            self.setParentItem(parent)

    def boundingRect(self):
        """
        The bounding rectangle of the PlotView is Null, so we need an "empty" bounding rect.
        """
        return self.plotBorder.boundingRect()

    def contextMenuEvent(self, event):
        """
        Overload the contextMenu so that we don't "steal" events from the plot widget.

        Args:
            event: QGraphicsSceneContextMenuEvent
        """
        if self.proxy.contains(event.pos()):
            pass
        else:
            super(PlotView, self).contextMenuEvent(event)

    def serialize(self):
        """
        Serialize the important parameters

        Returns:
            Python dictionary
        """
        pos  = self.plotBorder.scenePos()
        size = self.proxy.size()
        definition = super(PlotView, self).serialize()
        definition['kwargs'] = {'size': [size.width(), size.height()]}
        definition['position'] = (pos.x(),pos.y())
        return definition

    def updateView(self, status):
        """
        Update the view.

        Args:
            name: name associated with the status
            status:  status as a python dictionary
        """
        try:
            data = float(status[Task.CustomStatusKey])
            self.data.append(data)
            self.plot.setData(self.data)
        except:
            pass
