from pyqtlib import QtGui, QtCore, QT_LIB

from pyqtlib.node_graph import TextItem

from taskforce_common import Task
from taskforce_gui import TaskView
from taskforce_model import TaskForceColors

class StatusTextView(TaskView):
    """
    The StatusTextView is a simple label that can be associated with a Task.  Every update, the text will be set
    to the value of the status[Task.CustomStatusKey].  This is a simple way to send a single value to the TaskForce UI.
    """

    def __init__(self, parent=None):
        TaskView.__init__(self, parent=parent)
        self.textItem = TextItem('<status>')
        self.textItem.setParentItem(self)
        self.textItem.setEditable(False)
        self.textItem.contextMenuEvent = self.contextMenuEvent

        self.defaultTextColor = self.textItem.defaultTextColor()
        self.selectedTextColor = TaskForceColors.SelectedTextDefaultColor

        if QT_LIB is 'PyQt4':
            self.setHandlesChildEvents(True)

    def boundingRect(self):
        return self.textItem.boundingRect()

    def paint(self, painter, option, widget):
        """
        Override the text color if selected.
        """
        if self.isSelected():
            self.textItem.setDefaultTextColor(self.selectedTextColor)
        else:
            self.textItem.setDefaultTextColor(self.defaultTextColor)


    def updateView(self, status):
        self.textItem.setText(status.get(Task.CustomStatusKey, None))
