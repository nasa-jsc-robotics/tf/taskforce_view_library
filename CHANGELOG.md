Changelog
=========

1.0.5
----

* Made StatusTextView movable and able to be linked from GUI
* Removed trusty ci

1.0.4
-----

* Added NOSA LICENSE file

1.0.3
-----
* Added xenial ci

1.0.2
-----
* Added logic to work with PyQt4 and PyQt5
